package Model;

import java.io.Serializable;
import java.util.*;
/**
 * 
 * @author linhdang
 *
 */
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 130893L;
	/**
	 * Indicates if this user object is logged in
	 */
	public boolean Active;
	/**
	 * Username
	 */
	public String UserName;
	private ArrayList<Album> albumlist;

	/**
	 * Contains all of user's albums
	 */
	//public ArrayList<Album> Albums;

	/**
	 * Construct user, not active if user is created...
	 * @param UserName The username for the User
	 */
	public User(String UserName) {
		this.Active = false;
		this.UserName = UserName;
		albumlist = new ArrayList<Album>();
	}
	/**
	 * get user name
	 * @return user name
	 */
	public String getName() {
		return UserName;
	}
	/**
	 * get album list  method 
	 * @return an album list
	 */
	public ArrayList<Album> getAlbum() {
		return albumlist;
	}

	/**
	 * To string..... does two things in one.
	 */
	public String toString() {
		return UserName;
	}

}
