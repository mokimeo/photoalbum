package Model;

import java.io.Serializable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.scene.image.Image;

/**
 * 
 * @author Lynn Dang, Maury Johnson
 *
 */
public class Photo implements Serializable {
	public String filename;
	public String Caption;
	public String AbsolutePath;
	
	private static final int BADINPUT = -1;
	private static final int BADTAG = -2;
	public image image;
	Calendar date;
	public long lastmodifieddate;
	public ArrayList<Tag> Taglist = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = 121214214214L;
	private static final String SimpleDateFormat = null;
	/**
	 * Create a new photo 
	 * @param filename filename 
	 * @param AbsolutePath absolute path of filename
	 * @param image a image
	 * @param date date that uploads the image
	 * @param lastmodified parameter indicating the date the image is being used
	 */
	public Photo(String filename,String AbsolutePath, Image image, Calendar date, long lastmodified) {
		this.filename = filename;
		if(AbsolutePath==null) {
			System.err.printf("Error! Absolute Path to photo: %s does not exist!!",filename);
		}
		
		try {
		this.AbsolutePath=AbsolutePath.split("file:")[1];
		}
		catch(Exception F) {
			
			System.err.println("CAUGHT ABSOLUTE PATH ERR...");
			
			this.AbsolutePath=AbsolutePath;
		}
		lastmodifieddate = lastmodified;
		
		System.out.println(AbsolutePath);
		this.Caption = "";
		this.date = date;
		this.date.set(Calendar.MILLISECOND,0);
		this.image = new image();
		this.image.SetImage(image);
		
		Taglist = new ArrayList<Tag>();
		System.out.printf("NEW PHOTO: %s,%s,%s", this.filename,this.AbsolutePath,this.Caption);
	}
	
	/**
	 * toString
	 */
	public String toString() {
		return filename;
	}
	/**
	 * Add Caption 
	 * @param string Value of Caption
	 */
	public void AddCaption(String string ) {
		this.Caption = string;
	}
	/**
	 * Get caption
	 * @return caption
	 */
	public String getCaption() {
		return this.Caption;
	}
	/**
	 * get image 
	 * @return image serializef image
	 */
	public Image getImage() {
		return image.getImage();
	}
	
	/**
	 * get Date From Calendar
	 * @return date
	 */
	public Calendar getDate() {
		return date;
	}
	
	/**
	 * Use toString(int) to get photo details
	 * @param DummyNum some random number
	 * @return photo details
	 */
	public String toString(int DummyNum) {
		return PhotoDetails();
	}
	
	/**
	 * Check if date that photo was taken is within a date range
	 * @param fromDate date indicating where to start from
	 * @param toDate date indicating where to end
	 * @return boolean indicating if photo's date is within this date range
	 */
	public boolean isWithinDateRange(LocalDate fromDate, LocalDate toDate) {
		LocalDate date = getDate().getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		
		return date.isBefore(toDate) && date.isAfter(fromDate) || date.equals(fromDate) || date.equals(toDate);
	}
	
	
	/**
	 * 
	 * @return photo details
	 */
	public String PhotoDetails() {
		
		SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy 'at' hh:mm ");

		String Tags = TTS();
	
		String Stringdate = date.format(getDate().getTime());
		
		return "Filename: " + filename +"\nDate Taken:"+Stringdate+ "\nTags:\n" + Tags + "\nCaption: " + Caption;
	}

	/**
	 * Tags to String
	 * @return
	 */
	private String TTS() {
		// TODO Auto-generated method stub
		String Result = "";
		
		//Hashtable stores visited tags
		Hashtable<String,String> VisitedTags = new Hashtable<String,String>();
		
		for(int i=0; i<Taglist.size();i+=1) {
			
			//If first tags match, then found matching tags
			Tag t = Taglist.get(i);
			
			//Check if hashtable doesn't have the tags set already
			if(VisitedTags.get(t.TAAM[0])==null) {
			//Predicate used for filtering
			Predicate<Tag> AllMatchingTags = (T)->T.TAAM[0].equals(t.TAAM[0]);
			
			//Get all matching tags by filtering them out
			List<Tag> MatchingTags = Taglist.stream().filter(AllMatchingTags).collect(Collectors.toList());
			
			//Only need first tag, rest don't matter
			VisitedTags.put(MatchingTags.get(0).TAAM[0], "");
			
			//Add ALL Tags to result string
			for(Tag o:MatchingTags) Result+=o+"\n";
			
			if(i<Taglist.size()-1)
			Result+="\n";
			
			}
		}
		
		return Result;
	}
	
	/**
	 * 
	 * @param Tag Tag array
	 * @return integer value corresponding to if the Tag can be added
	 */
	public int AddTag(String[] Tag) {
		
		if(Tag.length>2 || Tag.length<2) {
			return BADINPUT;
		}
		if(NoDuplicateTagValuePair(Tag)){
			Taglist.add(new Tag(Tag));
			return 0;
		}
		else {
			return BADTAG;
		}
	}
	
	/**
	 * IF there is a bad tag input, returns false, otherwise
	 * return true
	 * @param tag the tag-value pair
	 * @return boolean indicating no duplicate tag value pair
	 */
	public boolean NoDuplicateTagValuePair(String[] tag) {
		List<Tag> Result = (
				Taglist.stream().filter(
				T->T.apply(T.TAAM, tag))
				)
				.collect(
				Collectors.toList()
				);
		return Result.size()>0 ? false:true;
	}
	
	
	/**
	 * MAKE SURE THAT PATH IS THE COPY OF PHOTO, NOT THE ACTUAL PHOTO!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * 1!!!!!!!!!!!!!!!!!!!!!!!!!!11
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * @param P the Photo Object
	 * @return If paths match
	 */
	public boolean equals(Photo P) {
		if(P.AbsolutePath==null || this.AbsolutePath==null) {
			return true;
		}
		return this.AbsolutePath.equals(P.AbsolutePath);
	}
	
}
