package Model;

import java.io.Serializable;
import java.util.*;
import java.util.function.BiFunction;

/**
 * 
 * @author Lynn Dang
 *
 */
public class Tag implements Serializable {
	String TagName;
	String TagValue;

		public class Tag(String TagName, String TagValue){
				this.TagName = TagName;
				this.TagValue = TagValue;
		}
		
		/**
		 * Return string format of Tag-Value Pair
		 */
		public String toString() {
		
		return this.TagName + "-" + this.TagValue;	
		
		}

}
