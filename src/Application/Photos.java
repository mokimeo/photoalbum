package Application;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import Controller.Controller;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

/**
 * 
 * @author Maury Johnson
 *
 */
public class Photos extends Application {

	/**
	 * Photos primary app
	 */
	//Stage level 0
	@Override
	public void start(Stage primaryStage) {
		try {

			FXMLLoader loader = new FXMLLoader();
			Pane root;
			Controller C = new Controller();

			loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/View/Login.fxml"));


			root = (Pane) loader.load();

			C = (Controller) loader.getController();

			Scene scene = new Scene(root);

			C.start(primaryStage);

			primaryStage.setScene(scene);
			primaryStage.setTitle("Login");
			primaryStage.setResizable(false);
			primaryStage.show();

			primaryStage.setOnCloseRequest(event -> {
				System.out.println("Closing Primary Stage");
			});

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
