package Controller;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import Model.Photo;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import Model.*;

/**
 * 
 * @author linhdang
 *
 */
public class SubAlbumController {

	@FXML
	ChoiceBox<String> AlbumChoice;
	@FXML
	Button Move, Copy;
	@FXML
	Button Logout;
	@FXML
	Button Back;
	/**
	 * Photo list view
	 */
	ListView<Photo> PhotoList;
	/**
	 * photo
	 */
	Photo SelectedPhoto;
	/**
	 * current album
	 */
	Album currentAlbum;
	/**
	 * current user
	 */
	User CurrentUser;
	/**
	 * Handle buttons
	 * @param event Action event
	 */
	public void HandleButton(ActionEvent event) {
		Button b = (Button) event.getSource();
		if (b == Move || b == Copy) {
			HandleMovePhoto(b);
		} else if (b == Back) {
			Controller.Stages.get(3).close();
			Controller.Serialization(Controller.Users);
			Controller.Stages.get(2).show();

		} else if (b == Logout) {
			Controller.Stages.get(3).close();
			Controller.Serialization(Controller.Users);
			Controller.Stages.get(0).show();
		}
	}
	/**
	 * Get ListView<Photo>
	 * @param photolist ListView<Photo> List of Photo objects
	 */
	public void getPhotoList(ListView<Photo> photolist) {
		PhotoList = photolist;
	}
	/**
	 * Get current album
	 * @param album a currentalbum in Albumcontroller
	 */
	public void GetCurrentAlbum(Album album) {
		currentAlbum = album;
	}
	/**
	 * Get Selected Photo
	 * @param photo return a current selectedphoto
	 */
	public void getSelectedPhoto(Photo photo) {
		SelectedPhoto = photo;
	}
	/**
	 * Handle Move + copy Photo
	 * @param b Button type
	 */
	public void HandleMovePhoto(Button b) {
		String selectedString = AlbumChoice.getSelectionModel().getSelectedItem();
		for (Album a : CurrentUser.getAlbum()) {
			if (a.getName().equals(selectedString)) {
				if (isDuplicatedPhoto(SelectedPhoto, a)) {
					Controller.InValidEntry(103);
					return;
				}
				a.getPhotoList().add(SelectedPhoto);
				if (b == Move) {
					currentAlbum.getPhotoList().remove(SelectedPhoto);
					PhotoList.getItems().remove(SelectedPhoto);
					PhotoList.refresh();
					PhotoList.getSelectionModel().select(0);
					Controller.Serialization(Controller.Users);
				}
				AlbumChoice.setValue("b");
				Controller.Serialization(Controller.Users);

			}
		}
	}
	/**
	 * Check if current photo is duplicated
	 * @param photo a selected photo
	 * @param album a current album
	 * @return boolean value corresponding to value of photo
	 */
	public boolean isDuplicatedPhoto(Photo photo, Album album) {
		if (album.getPhotoList().size() > 0) {
			for (int i = 0; i < album.getPhotoList().size(); i++) {
				if (album.getPhotoList().get(i).equals(photo)) {
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * initialize ChoiceBox
	 * @param user current user
	 */
	public void start(User user) {

		ArrayList<String> StringAlbum = new ArrayList<String>();
		StringAlbum.add(0, "");
		ArrayList<Album> AlbumList = user.getAlbum();
		for (Album a : AlbumList) {
			StringAlbum.add(a.getName());
		}
		ObservableList<String> obsAlbum = FXCollections.observableArrayList(StringAlbum);
		CurrentUser = user;
		AlbumChoice.setItems(obsAlbum);
		AlbumChoice.setValue("Select Album");

	}

}
