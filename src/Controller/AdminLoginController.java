package Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import Model.User;
import Model.Album;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 * 
 * @author Maury Johnson
 *
 */
public class AdminLoginController{

	/**
	 * Button indicating Adding a User
	 */
	@FXML
	Button AddUser;
	/**
	 * Button indicating Deleting a User
	 */
	@FXML
	Button DeleteUser;
	//@FXML
	//Button ViewUser;
	/**
	 * Button indicating Admin Logging Out
	 */
	@FXML
	Button AdminLogout;
	
	/**
	 * TextField indicating Username textbox
	 */
	@FXML
	TextField UserName;

	/**
	 * Viewable List of users the admin should have
	 */
	@FXML
	public  ListView<User> UserList;
	 
	/**
	 * Serializable list of users the admin should have
	 */
	private static ArrayList<User> UserARList = new ArrayList<User>();
	
	/**
	 * Indicates user selected IFF username is selected in listview
	 */
	String UserSelected = null;
	
	/**
	 * Indicates of deleting the user for admin
	 */
	public  boolean DeletingUser;

	/**
	 * Populate ListView of Users for Admin
	 */
	 void Populate() {
		 
			UserARList = Controller.Deserialization();
			if(UserARList == null) {
				UserARList = new ArrayList<User>();
			}
			UserList.setItems(FXCollections.observableArrayList(UserARList));
			UserList.refresh();
	}
	 /**
	  * Deserialization
	  * @return Returns ArrayList of Users
	  */
	 @SuppressWarnings("unchecked")
		public ArrayList<User> Deserialization() {
			ArrayList<User> users = null;
			

			try {
				FileInputStream file = new FileInputStream(System.getProperty("user.dir") + "/" + "UserAlbum.ser");
				ObjectInputStream in = new ObjectInputStream(file);
				 users = (ArrayList<User>) in.readObject();

				in.close();
				file.close();

				System.out.println("Object has been deserialized ");
				return users;
			}

			catch (IOException ex) {
				System.out.println("IOException is caught");
			}

			catch (ClassNotFoundException ex) {
				System.out.println("ClassNotFoundException is caught");
			}
			return null;
		}
	 /**
	  * Convert action e to an action
	  * @param e Action Event
	  */
	public void convert(ActionEvent e) {
		Button b = (Button) e.getSource();
		if (b == AddUser) {
			System.out.printf("\n Add User button pressed, for user:%s\n", UserName.getText());
			HandleAddUser();
		} else if (b == DeleteUser) {
			System.out.printf("\n Delete User button pressed, for user:%s\n", UserName.getText());
			HandleDeleteUser();
		} 
		else if (b == AdminLogout) {
			Controller.Stages.get(1).close();
			Controller.Stages.get(0).show();
		}
		
	}

	/**
	 * Handle deleting user
	 */
	private void HandleDeleteUser() {
		// TODO Auto-generated method stub
		if(UserSelected==null) {
			UserSelected = UserName.getText();
			
			
			//WOW... LET ME PUSH
			
		}
		
			if(UserName.getText()!=null) {
			if(UserName.getText().length()>0) {
			
				if(UserName.getText().compareTo("stock")==0) {
					InvalidParam(4);
					return;
				}
				
			//Sxy lambdas
				
			Predicate<User> PotentialUser = (U) -> U.UserName.equals(UserName.getText());
			List<User> DummyList = (UserARList.stream().filter(PotentialUser)).collect(Collectors.toList());
			
			if(DummyList.size()>0) {
			UserList.getSelectionModel().clearSelection();
			UserList.getItems().removeIf((U)->U.UserName.equals(UserName.getText()));
			UserARList.removeIf((U)->U.UserName.equals(UserName.getText()));
			Controller.Serialization(UserARList);
			UserList.refresh();
			}
			else {
				InvalidParam(3);
			}
			}
			else {
				InvalidParam(2);
			}
			}
			else {
				System.out.println("UserName TextField Does Not Exist!");
				System.exit(-1);
			}
		
	}

	/**
	 * Invalid parameter cases, gives alertbox
	 * @param errno Errno indicating many types of user errors
	 */
	private void InvalidParam(int errno) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Validate Fields");
		alert.setHeaderText(null);
		if(errno==1) {
			alert.setContentText("Username Already Exists! Please Try again !");
		}
		else if(errno==2) {
			alert.setContentText("Field Empty ! Please Try Again !");
		}
		else if(errno==3) {
			alert.setContentText("Username Does Not Exist!");
		}
		else if(errno==4) {
			alert.setContentText("Cannot Delete stock User");
		}
		alert.showAndWait();
	}
	
	/**
	 * Handle adding new user
	 */
	private void HandleAddUser() {
		// TODO Auto-generated method stub
		if(UserName.getText()!=null) {
		if(UserName.getText().length()!=0) {
			
			/*
			if(UserSelected!=null) {
			if(UserName.getText().compareTo(UserSelected)!=0) {
				UserSelected = UserName.getText();
			}
			}
			*/
			
		Predicate<User> C = (U) -> U.UserName.compareTo(UserName.getText())==0;

		List<User> UserGot = (UserARList.stream().filter(C)).collect(Collectors.toList());
	
		if(UserGot.size()==0) {
		
		UserARList.add(new User(UserName.getText()));		
			
		UserList.getItems().add(new User(UserName.getText()));
		
		UserList.getSelectionModel().clearSelection();
		
		Controller.Serialization(UserARList);
		UserList.refresh();
		}
		else {
			InvalidParam(1);
		}
		}
		else {
			InvalidParam(2);
		}
		}
		else {
			System.out.println("UserName TextField Does Not Exist!");
			System.exit(-1);
		}
	}
	
}
