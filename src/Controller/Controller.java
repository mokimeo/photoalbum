package Controller;

import javafx.collections.*;
import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.stage.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

//import View.*

//For Lists..
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import Model.User;

/**
 * Handle all controllers, the main controller
 * @author Lynn Dang, Maury Johnson
 */
public class Controller {
	/**
	 * Parameter Indicating Invalid Entry
	 */
	final static int InvalidEntry = 100;
	/**
	 * Parameter Indicating Empty Field
	 */
	final static int EmptyField = 101;
	/**
	 * Parameter Indicating Duplicated Album
	 */
	final static int DuplicatedAlbum = 102;
	/**
	 * Parameter indicating Duplicated Photo
	 */
	final static int DuplicatedPhoto = 103;
	/**
	 * List of all users contains username, Active, Albums THERE can only be one
	 * Arraylist that stores the correct users.
	 */
	static ArrayList<User> Users = new ArrayList<User>();

	/**
	 * Username textfield
	 */
	@FXML
	TextField UserName;
	/**
	 * Login Button
	 */
	@FXML
	Button LoginButton;

	/**
	 * Pointer to ALL WORKING STAGES upon start
	 */
	//public static Stage St;
	public static ArrayList<Stage> Stages = new ArrayList<Stage>();
	
	/**
	 * Set the stage
	 * @param MainStage The Main Stage
	 */
	public void start(Stage MainStage) {
		

		Stages.add(MainStage);
		/**
		 * Check if user used enter
		 */
		try {

			// Detect username enter, get text from it
			UserName.setOnKeyPressed((event) -> {
				if (event.getCode() == KeyCode.ENTER) {
					// Case where user entered enter, either go to user window
					// Or go to admin window
					System.out.printf("Login Enter key pressed, username: %s\n", UserName.getText());
					Login();
				}
			});

		} catch (Exception J) {
			J.printStackTrace();
			System.exit(-1);
		}
	}

	/**
	 * Add stage to list of stages based on type of stage to add!
	 * @param Type Type of stage to create
	 */
	public static void AddStage(int Type) {
		//Stage already added, recycle stage
		if(Type==Stages.size()-1) {
			
			Stages.set(Type, new Stage());
			
		}
		else {
			
			Stages.add(new Stage());
			
		}
	}
	
	/**
	 * Convert action for Login Stage to some action...
	 * 
	 * @param e action event
	 */
	public void convert(ActionEvent e) {
		Button b = (Button) e.getSource();
		if (b == LoginButton) {
			System.out.println("\nLogin button pressed");
			Login();
		}
	}

	/**
	 * Deserialize File 1
	 * @return ArrayList of Users
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<User> Deserialization() {
		ArrayList<User> users = null;
		

		try {
			
			/*
			ArrayList<User> DummyList = Controller.Users.stream().filter(U->U.UserName.compareTo("stock")==0).collect(Collectors.toCollection(ArrayList<User>::new));
			if(DummyList==null) {
				Controller.Users.add(new User("stock"));
				Serialization(Controller.Users);
			}
			else if(DummyList.size()==0) {
				Controller.Users.add(new User("stock"));
				Serialization(Controller.Users);
			}
			*/
			
			FileInputStream file = new FileInputStream(System.getProperty("user.dir") + "/" + "UserAlbum.ser");
			ObjectInputStream in = new ObjectInputStream(file);
			users = (ArrayList<User>) in.readObject();

			in.close();
			file.close();

			System.out.println("Object has been deserialized ");
			return users;
		}

		catch (IOException ex) {
			System.out.println("IOException is caught");
		}

		catch (ClassNotFoundException ex) {
			System.out.println("ClassNotFoundException is caught");
		}
		return null;
	}

	/**
	 * Login to user
	 */
	public void Login() {
		
		System.out.println("login");
		
		if (UserName.getText().equals("")) {
			InValidEntry(EmptyField);
			return;
		}
		if (UserName.getText().equals("Admin")) {
			// Call Admin Window

			try {
				UserName.clear();
				FXMLLoader loader = new FXMLLoader();

				System.out.printf("THIS CLASS: %s", getClass().getName());

				loader.setLocation(getClass().getResource("/View/Admin.fxml"));

				Pane root = (Pane) loader.load();

				AdminLoginController A = loader.<AdminLoginController>getController();

				Scene scene = new Scene(root);
				AddStage(1);
				
				Stages.get(1).setOnCloseRequest(event -> {
					System.out.println("Closing Admin Stage");
					Stages.get(1).close();
					Serialization(Users);
					Stages.get(0).close();
				});

				// POPULATE USER LIST!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				A.Populate();

				A.UserList.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<User>() {
					public void onChanged(ListChangeListener.Change<? extends User> c) {
						// GET USER!!!!!!!!!!!!!!
						String[] U1 = c.toString().split(" replaced by ");
						String User = U1.length > 1 ? U1[1].substring(1, U1[1].indexOf("]"))
								: U1[0].substring(U1[0].indexOf("[") + 1, U1[0].indexOf("]"));
						System.out.printf("\n%s User Selected: %s\n", c, User);
						A.UserSelected = User;
						A.UserName.setText(User);
					}
				});

				Stages.get(0).hide();
				Stages.get(1).setScene(scene);
				Stages.get(1).showAndWait();

			} catch (Exception e) {
				e.printStackTrace();
				System.exit(-1);
			}

			return;

		} else {
			System.out.println("user");
			Users = Deserialization();
			if(Users == null) {
				InValidEntry(InvalidEntry);
				return;
			}
			User CurrentUser = null;
			
			if (Users.size() > 0) {
				int i = 0;
				
				for ( i = 0; i < Users.size(); i++) {
					if (Users.get(i).getName().equals(UserName.getText())) {
						CurrentUser = Users.get(i);
						break;
					}
				}
				if(i == Users.size()) {
					InValidEntry(InvalidEntry);
					return;
				}
			} else {
				InValidEntry(InvalidEntry);
				return;
			}
			
			//LOL replace 2 liner with 10 -_- =)
			
			// Predicate<User> C = (U) -> U.UserName.equals(UserName.getText());

			// List<User> UserGot = (Users.stream().filter(C)).collect(Collectors.toList());

				try {
					UserName.clear();
					FXMLLoader loader = new FXMLLoader();

					System.out.printf("THIS CLASS: %s", getClass().getName());

					loader.setLocation(getClass().getResource("/View/User.fxml"));
					Pane root = (Pane) loader.load();
					UserLoginController controller = loader.<UserLoginController>getController();

					Scene scene = new Scene(root);
					AddStage(1);
					
					controller.ShowAlbum(CurrentUser);

					Stages.get(1).setScene(scene);
					
					Stages.get(1).setOnCloseRequest(event -> {
						System.out.println("Closing Admin Stage");
						Stages.get(1).close();
						Serialization(Users);
						Stages.get(0).close();
					});
					
					Stages.get(1).show();
					Stages.get(0).hide();
					
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(-1);
				}
			}
	}

	/**
	 * Handle invalid entry, different cases, show alert box
	 * @param errno parameter indicating various errors
	 */
	public static void InValidEntry(int errno) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Validate Fields");
		alert.setHeaderText(null);
		if (errno == InvalidEntry) {
			alert.setContentText("User Name doesn't exist, Please Ask Admin to sign up!");
		} else if (errno == EmptyField) {
			alert.setContentText("Field Empty ! Please Try Again !");
		}else if (errno == DuplicatedAlbum) {
			alert.setContentText("Duplicated Album ! Please Try Again !");
		} else if (errno == DuplicatedPhoto) {
			alert.setContentText("Duplicated Photo !");
		}
		alert.showAndWait();

	}

	/**
	 * Deprecated, checks fxml files for fx:controller
	 * @param Criteria criteria to determine if file is changes
	 * @return Return boolean indicating of the file was changed/altered
	 */
	public static boolean FileChanged(String Criteria) {

		String[] FC = new String[] { "fx:controller", "fx: controller", "fx:Controller", "fx: Controller" };

		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();

		System.out.printf("\nPath: %s\\src\\View\\%s\n", s, Criteria);

		File file = new File(s + "\\src\\View\\" + Criteria);

		try {
			Scanner scanner = new Scanner(file);
			// now read the file line by line...
			int lineNum = 0;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				lineNum++;
				// System.out.println(line);
				if (line.contains(FC[0]) || line.contains(FC[1]) || line.contains(FC[2]) || line.contains(FC[3])) {
					return true;
				}
			}
		} catch (FileNotFoundException e) {
			// handle this
			e.printStackTrace();
			System.exit(-1);
		}
		return false;
	}

	/**
	 * Serialize File 1
	 * @param userlist The list of User objects
	 */
	public static void Serialization(ArrayList<User> userlist) {
		try
        {    
			
         
            FileOutputStream file = new FileOutputStream(System.getProperty("user.dir")+"/"+"UserAlbum.ser"); 
            ObjectOutputStream out = new ObjectOutputStream(file); 

            out.writeObject(userlist); 
              
            out.close(); 
            file.close(); 
              
            System.out.println("Object has been serialized"); 
  
        } 
          
        catch(IOException ex) 
        { 
            System.out.println("IOException is caught"); 
        } 
	}

}
