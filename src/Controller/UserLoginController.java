package Controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Optional;

import Application.Photos;
import Model.User;

import Model.Album;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 * 
 * @author linhdang, Maury Johnson
 *
 */

public class UserLoginController {
	int OpenAlbum = 10;
	int SearchPhoto = 11;
	int EmptyField = 100;
	int DuplicatedField = 101;
	@FXML
	TextField AlbumTextField;
	@FXML
	Button RenameAlbumButton;
	@FXML
	Button DeleteAlbumButton;
	@FXML
	Button AlbumTextFieldButton;
	@FXML
	Button CreateAlbumButton;
	@FXML
	Button OpenAlbumButton;
	@FXML
	Button SearchPhotoButton;
	@FXML
	Button LogoutButton;
	@FXML
	Label UserNameLabel;
	@FXML
	private ListView<Album> AlbumList;
	private ArrayList<User> UserList;
	private ArrayList<Album> AlbumAr = new ArrayList<Album>();
	public static User currentuser;
	boolean NewStage = false;
	// Stage level 1

	/**
	 * Show the current list of Album user is having
	 * 
	 * @param user user
	 */
	public void ShowAlbum(User user) {
		System.out.println("...." + user.getName());
		currentuser = user;
		UserList = Controller.Users;
		AlbumAr = user.getAlbum();
		AlbumList.setItems(FXCollections.observableArrayList(currentuser.getAlbum()));
		AlbumList.getSelectionModel().select(0);
		UserNameLabel.setText(user.getName() + "!");
	}

	/**
	 * Handle Logout Button
	 * 
	 * @param closebutton close button
	 */
	public void HandleLogout(Button closebutton) {

		try {
			if (NewStage == false) {
				Controller.Stages.get(1).close();
				Controller.Stages.get(0).show();
			}else {
				
				System.out.println("stage size:"+Controller.Stages.size());
				Controller.Stages.get(2).close();
				Controller.Stages.get(0).show();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Chekc if album is a duplicate
	 * 
	 * @param album Album Object
	 * @return boolean indicating there is a duplicated album
	 */
	public boolean isDuplicatedAlbum(String album) {
		ObservableList<Album> obsAlbumList = AlbumList.getItems();
		for (Album CurrentAlbum : obsAlbumList) {
			if (CurrentAlbum.getName().equals(album)) {

				return true;
			}
		}
		return false;
	}

	/**
	 * Handles adding album
	 * 
	 * @param album the Album object
	 */
	public void AddAlbum(Album album) {
		if (AlbumAr.size() > 0) {
			if (isDuplicatedAlbum(album.getName())) {
				Controller.InValidEntry(Controller.DuplicatedAlbum);
				return;
			}
		}
		currentuser.getAlbum().add(album);

		AlbumList.setItems(FXCollections.observableArrayList(currentuser.getAlbum()));

		AlbumList.getSelectionModel().select(album);
		AlbumList.refresh();
	}

	/**
	 * Handles creating album
	 */
	public void HandleCreateAlbum() {
		String input = AlbumTextField.getText();

		if (input.equals("")) {
			Controller.InValidEntry(Controller.EmptyField);
			return;
		}
		Album newalbum = new Album(input);

		Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle("Comfirmation Dialog");
		alert.setHeaderText("Are you sure to Add?");
		Optional<ButtonType> action = alert.showAndWait();
		if (action.get() == ButtonType.CANCEL) {
			AlbumTextField.clear();
			return;
		}
		if (action.get() == ButtonType.OK) {
			AddAlbum(newalbum);
			Serialization(UserList);
			AlbumTextField.clear();
		}
	}

	/**
	 * Handles renaming album
	 */
	public void HandleRenameAlbum() {
		String input = AlbumTextField.getText();
		if (input.equals("")) {
			Controller.InValidEntry(Controller.EmptyField);
			return;
		}
		Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle("Comfirmation Dialog");
		alert.setHeaderText("Are you sure to edit?");
		Optional<ButtonType> action = alert.showAndWait();
		if (action.get() == ButtonType.CANCEL) {
			AlbumTextField.clear();
			return;
		}
		if (action.get() == ButtonType.OK) {
			if (isDuplicatedAlbum(input)) {
				Controller.InValidEntry(Controller.DuplicatedAlbum);
				return;
			}
			AlbumList.getSelectionModel().getSelectedItem().EditName(input);
			AlbumList.refresh();
			AlbumTextField.clear();
		}
		Serialization(UserList);

	}

	/**
	 * Handles deleting album
	 */
	public void HandleDeleteAlbum() {
		int index = AlbumList.getSelectionModel().getSelectedIndex();
		Album item = AlbumList.getSelectionModel().getSelectedItem();
		if (index < 0)
			return;

		Alert alert = new Alert(AlertType.CONFIRMATION);

		alert.setTitle("Comfirmation Dialog");
		alert.setHeaderText("Are you sure to delete?");
		Optional<ButtonType> action = alert.showAndWait();
		if (action.get() == ButtonType.CANCEL) {
			return;
		}
		if (action.get() == ButtonType.OK) {

			currentuser.getAlbum().remove(item);
			AlbumList.getItems().remove(item);
			AlbumList.refresh();

		}
		Serialization(UserList);
	}

	/**
	 * Serialization
	 * 
	 * @param userlist user list
	 */
	public void Serialization(ArrayList<User> userlist) {
		try {

			FileOutputStream file = new FileOutputStream(System.getProperty("user.dir") + "/" + "UserAlbum.ser");
			ObjectOutputStream out = new ObjectOutputStream(file);

			out.writeObject(userlist);

			out.close();
			file.close();

			System.out.println("Object has been serialized");

		}

		catch (IOException ex) {
			System.out.println("IOException is caught");
		}
	}

	public void HandleOpenAlbum(int open, Button b) {
		PopUpPane(open, b);
	}

	public void HandleSearchPhoto(int search, Button b) {
		PopUpPane(search, b);
	}

	/**
	 * 
	 * NOTE - Updates somethings, because I made list of Stages
	 * 
	 * @param input a paramater corresponding to button type
	 * @param b     a button
	 */
	public void PopUpPane(int input, Button b) {
		Album item = null;
		String FXMLPath = null;
		if (input == OpenAlbum) {
			FXMLPath = "/View/Open Album.fxml";
			item = AlbumList.getSelectionModel().getSelectedItem();
			int index = AlbumList.getSelectionModel().getSelectedIndex();
			if (index < 0)
				return;
		} else {
			FXMLPath = "/View/Search Photo.fxml";
		}

		try {

			FXMLLoader loader = new FXMLLoader();

			loader.setLocation(getClass().getResource(FXMLPath));
			Pane root = (Pane) loader.load();

			if (input == OpenAlbum) {
				AlbumController controller = loader.<AlbumController>getController();
				controller.album = item;
				controller.ViewPhotoList(currentuser, item);
				Stage stage = (Stage) b.getScene().getWindow();
				stage.close();
				controller.start(new Scene(root));
				return;
			}

			else if (input == SearchPhoto) {
				PhotoSearch controller = loader.<PhotoSearch>getController();
				Stage stage = (Stage) b.getScene().getWindow();
				stage.close();
				controller.MyUser = currentuser;
				controller.start(new Scene(root), currentuser);
				return;
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/**
	 * Convert button function
	 * 
	 * @param event Action event
	 */
	public void convert(ActionEvent event) {
		Button b = (Button) event.getSource();
		if (b == AlbumTextFieldButton) {
			System.out.println("\nAdd Album button pressed");

		} else if (b == DeleteAlbumButton) {
			System.out.println("\nDelete Album button pressed");
			HandleDeleteAlbum();

		} else if (b == RenameAlbumButton) {
			System.out.println("\nRename Album button pressed");
			HandleRenameAlbum();
		} else if (b == OpenAlbumButton) {
			HandleOpenAlbum(OpenAlbum, b);

		} else if (b == CreateAlbumButton) {
			System.out.println("\nCreate Album button pressed");
			HandleCreateAlbum();
		} else if (b == SearchPhotoButton) {
			HandleSearchPhoto(SearchPhoto, b);
		} else if (b == LogoutButton) {
			HandleLogout(b);
		}
	}

	/**
	 * Set The Stage to a new stage
	 */
	public void SetStage() {
		NewStage = true;
	}

}
